﻿using System;
using Mesozoic;
using System.Collections.Generic;

namespace MesozoicHorde
{
    public class Horde
    {
        private List<Dinosaur> dinos = new List<Dinosaur>();
        private string name;


        public Horde(string name)
        {
            this.name = name;
        }
        public string getName()
        {
            return this.name;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public void HordeAdd(Dinosaur dino)
        {
            this.dinos.Add(dino);
        }
        public void HordeRemove(Dinosaur dino)
        {
            this.dinos.Remove(dino);
        }
        public string HordeMember()
        {
            string retour = "";
            foreach (Dinosaur dino in this.dinos)
            {
                retour = string.Format("{0}\n{1}", retour, dino.getName());
            }
            return retour;
        }
        public string sayHello()
        {
            string retour = "";
            foreach (Dinosaur dino in this.dinos)
            {
                retour = string.Format("{0}\n{1}", retour, dino.sayHello());
            }
            return retour;
        }
    }
}