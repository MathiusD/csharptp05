﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;
using MesozoicHorde;

namespace MesozoicConsole
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur yann = new Dinosaur("Yann", "Iguanodon", 8);
            Dinosaur theo = new Dinosaur("Théo", "Tyrannosaurus Rex", 10);
            Dinosaur thomas = new Dinosaur("Thomas", "Tyrannosaurus Rex", 5);
            Dinosaur patrick = new Dinosaur("Patrick", "Parasaurolophus", 8);
            Dinosaur patrick_2 = new Dinosaur("Patrick", "Parasaurolophus", 27);
            Dinosaur patrick_2_j = new Dinosaur("Patrick Junior", "Parasaurolophus", 11);
            Dinosaur clement = new Dinosaur("Clément", "Carnosaurus", 24);
            Dinosaur mathilde = new Dinosaur("Mathilde", "Miasasaure", 7);
            Dinosaur allan = new Dinosaur("Allan", "Allosaurus", 32);
            Dinosaur didier = new Dinosaur("Didier", "Deynonichus", 7);
            Dinosaur ronald = new Dinosaur("Ronald", "Vélociraptor", 8);
            Dinosaur roland = new Dinosaur("Roland", "Vélociraptor", 12);
            Dinosaur robin = new Dinosaur("Robin", "Vélociraptor", 14);
            Dinosaur jeanbaudouin = new Dinosaur("Jean-Baudouin", "Brachiosaurus", 34);
            Dinosaur spouatch = new Dinosaur("Spouatch", "Spinosaurus", 33);
            Dinosaur dori = new Dinosaur("Doritos", "Tricératops", 36);
            Horde griffes = new Horde("Les Griffes Sanglantes");
            Horde cornus = new Horde("Les Cornus");
            cornus.HordeAdd(louis);
            cornus.HordeAdd(nessie);
            cornus.HordeAdd(yann);
            cornus.HordeAdd(patrick);
            cornus.HordeAdd(patrick_2);
            cornus.HordeAdd(patrick_2_j);
            cornus.HordeAdd(mathilde);
            cornus.HordeAdd(jeanbaudouin);
            cornus.HordeAdd(dori);
            griffes.HordeAdd(theo);
            griffes.HordeAdd(thomas);
            griffes.HordeAdd(clement);
            griffes.HordeAdd(allan);
            griffes.HordeAdd(didier);
            griffes.HordeAdd(ronald);
            griffes.HordeAdd(roland);
            griffes.HordeAdd(robin);
            griffes.HordeAdd(spouatch);
            Console.WriteLine("Louis est :");
            Console.WriteLine(louis.getName());
            Console.WriteLine(louis.getSpecie());
            Console.WriteLine(louis.getAge());
            Console.WriteLine(nessie.sayHello());
            Console.WriteLine(nessie.roar());
            Console.WriteLine(nessie.hug(louis));
            Console.WriteLine(cornus.getName());
            Console.WriteLine(cornus.HordeMember());
            Console.WriteLine(cornus.sayHello());
            Console.WriteLine(griffes.getName());
            Console.WriteLine(griffes.HordeMember());
            Console.WriteLine(griffes.sayHello());
            Console.ReadKey();
        }
    }
}
