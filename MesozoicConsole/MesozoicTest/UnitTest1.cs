﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicConsole;
using Mesozoic;
using MesozoicHorde;

namespace MesozoicTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        [TestMethod]
        public void TestDinosaurModification()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            louis.setName("Nessie");
            louis.setSpecie("Diplodocus");
            louis.setAge(11);

            Assert.AreEqual("Nessie", louis.getName());
            Assert.AreEqual("Diplodocus", louis.getSpecie());
            Assert.AreEqual(11, louis.getAge());
        }
        [TestMethod]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }
        [TestMethod]
        public void TestHordeConstructor()
        {
            Horde cornus = new Horde("Les Cornus");

            Assert.AreEqual("Les Cornus", cornus.getName());
        }

        [TestMethod]
        public void TestHordSetName()
        {
            Horde cornus = new Horde("Les Cornus");

            Assert.AreEqual("Les Cornus", cornus.getName());
            cornus.setName("Les Griffes Sanglantes");
            Assert.AreEqual("Les Griffes Sanglantes", cornus.getName());

        }

        [TestMethod]
        public void TestHordeAdd()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Horde cornus = new Horde("Les Cornus");
            cornus.HordeAdd(louis);
            Assert.AreEqual("\nLouis", cornus.HordeMember());
            cornus.HordeAdd(nessie);
            Assert.AreEqual("\nLouis\nNessie", cornus.HordeMember());
        }

        [TestMethod]
        public void TestHordeRemove()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Horde cornus = new Horde("Les Cornus");
            cornus.HordeAdd(louis);
            Assert.AreEqual("\nLouis", cornus.HordeMember());
            cornus.HordeAdd(nessie);
            Assert.AreEqual("\nLouis\nNessie", cornus.HordeMember());
            cornus.HordeRemove(louis);
            Assert.AreEqual("\nNessie", cornus.HordeMember());
        }

        [TestMethod]
        public void TestHordeSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Horde cornus = new Horde("Les Cornus");
            cornus.HordeAdd(louis);
            Assert.AreEqual("\nJe suis Louis le Stegausaurus, j'ai 12 ans.", cornus.sayHello());
            cornus.HordeAdd(nessie);
            Assert.AreEqual("\nJe suis Louis le Stegausaurus, j'ai 12 ans.\nJe suis Nessie le Diplodocus, j'ai 11 ans.", cornus.sayHello());
        }
    }
}
